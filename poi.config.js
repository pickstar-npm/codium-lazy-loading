module.exports = {
  entry: './src/CodiumLazyLoading.vue',
  filename: {
    js: 'codium-lazy-loading.js',
  },
  sourceMap: false,
  html: false,
  format: 'cjs'
}
