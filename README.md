## Lazy Loading

A Vue wrapper component for Lazy Loading scrolling.

### Installation

Add package via Yarn:

```
$ yarn add git+ssh://git@code.codium.com.au:vue-components/lazy-loading.git
```

### Usage

Import the component where you need it:

```javascript
import LazyLoading from 'codium-lazy-loading'
```

Define it in your components list:

```javascript
components: {
  LazyLoading
}
```

Use it in your templates:

```html
<lazy-loading :loading="loading" :reached-bottom="retrieveMoreData" ref="lazyLoading">
  <strong slot="finished" class="text-muted">There are no more results.</strong>
</lazy-loading>
```

#### Options

##### `loading`

Type: `boolean`
Default: `false`
Required `false`

Pass your loading variable that indicates whether the app is loading, into the component. This will be watched by the component
and then more data will be loaded if required.

##### `reachedBottom`

Type: `function`
Required: `true`

The function that will get called when the component is in view, i.e. when scrolled to the bottom of the page.
Add your function to retrieve more data.

#### Install Component Globally

You can install the component globally so it does not need to be done for every component you use it. In a main file:

```javascript
import Vue from 'vue'
import LazyLoading from 'codium-lazy-loading'

Vue.component('lazy-loading', LazyLoading)
```
